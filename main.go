package main

import (
	"encoding/json"
	"fmt"

	productsinfo "go-gorm/products_info"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type SelectProduct struct {
	ID          int
	Name        string
	Model       string
	Price       float64
	Amount      int
	Category    productsinfo.Categories
	Type        productsinfo.Types
	Stores_list []productsinfo.Stores
}

func main() {
	fmt.Println("Hello world!")
	dbURL := "host=localhost port=5432 user=:) password=;) dbname=test sslmode=disable"
	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
	}

	db.AutoMigrate(&productsinfo.Types{})
	db.AutoMigrate(&productsinfo.Categories{})
	db.AutoMigrate(&productsinfo.Stores{})
	db.AutoMigrate(&productsinfo.Addresses{})
	db.AutoMigrate(&productsinfo.Products{})

	// db.Create(&productsinfo.Products_list)
	// types := []productsinfo.Types{
	// 	{
	// 		Name: "Meat",
	// 	},
	// 	{
	// 		Name: "Car",
	// 	},
	// 	{
	// 		Name: "Phone",
	// 	},
	// 	{
	// 		Name: "Jeans",
	// 	},
	// }
	// categories := []productsinfo.Categories{
	// 	{
	// 		Name: "Food",
	// 	},
	// 	{
	// 		Name: "Auto",
	// 	},
	// 	{
	// 		Name: "Electronic",
	// 	},
	// 	{
	// 		Name: "Output",
	// 	},
	// }
	// db.Create(&types)
	// db.Create(&categories)
	// db.Create(&productsinfo.Products_list)
	// product := []productsinfo.Products{}
	// product := []productsinfo.Products{}
	// res := db.Find(&product)
	// if res.Error != nil {
	// 	panic(res.Error)
	// }
	// for _, p := range product {
	// 	show(p)
	// }

	// Transaction example
	tr := db.Session(&gorm.Session{SkipDefaultTransaction: true})
	res := tr.Create(&productsinfo.Product)
	if res.Error != nil {
		fmt.Println("Transaction failed")
		tr.Rollback()
		panic(res.Error)
	}
	tr.Commit()

	GetAllProductsInfo(dbURL)
}

func show(p interface{}) {
	data, err := json.MarshalIndent(p, "", "   ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(data))
}

func GetAllProductsInfo(dbURL string) {
	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	products := []productsinfo.Products{}

	res := db.Table("products").Find(&products)
	if res.Error != nil {
		panic(res.Error)
	}

	for _, p := range products {
		selProduct := SelectProduct{}
		res = db.Table("categories").Select("id", "name").Find(&selProduct.Category, "id = ?", p.CategoryID)
		if res.Error != nil {
			panic(err)
		}
		selProduct.ID = p.ID
		selProduct.Amount = p.Amount
		selProduct.Name = p.Name
		selProduct.Model = p.Model
		selProduct.Price = p.Price
		res = db.Table("types").Select("id", "name").Find(&selProduct.Type, "id = ?", p.TypeID)
		if res.Error != nil {
			panic(res.Error)
		}
		// getting stores
		tempStores := []productsinfo.Stores{}
		res := db.Table("stores").Joins(`JOIN stores_products 
		as sp on stores.id=sp.stores_id 
		where sp.products_id=?`, selProduct.ID).Find(&tempStores)
		if res.Error != nil {
			panic(res.Error)
		}
		for _, s := range tempStores {
			res = db.Table("addresses").Find(&s.Addresses_list, "store_id = ?", s.ID)
			if res.Error != nil {
				panic(res.Error)
			}
			selProduct.Stores_list = append(selProduct.Stores_list, s)
		}
		show(selProduct)
	}
}
